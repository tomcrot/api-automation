const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const env = require('dotenv').config();
const domain = supertest(process.env.API_BASE_URL);

var Path = "/subscription/me";
var globals = require('./../../object/globals.js');

var putAPI = function(body, response) {
    domain.put(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })

}

module.exports = {
    putAPI: putAPI

};
