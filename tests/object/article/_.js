const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const env = require('dotenv').config();
const domain = supertest(process.env.API_BASE_URL);
var globals = require('./../globals.js');

var Path = "/article/5145881";
var Pathh = "/article/5145abc";
var topStories = "/feed/top_stories?detail=true";
var category = "/feed/category:58";
var geolocation = "/feed/geolocation";
var sourceFeed = "/feed/source:58";
var searchXmax = "/feed/search:xmax";
var trend = "/feed/trend:15";
var bookmarked = "/article/5145881/bookmark";


var getTopStories = function(response) {
    domain.get(topStories)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var getCategory = function(response) {
    domain.get(category)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var getGeolocation = function(response) {
    domain.get(geolocation)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var getSourceFeed = function(response) {
    domain.get(sourceFeed)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var getSearchXmax = function(response) {
    domain.get(searchXmax)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var getTrending = function(response) {
    domain.get(trend)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var postBookmark = function(body, response) {
    domain.post(bookmarked)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

var unBookmark = function(body, response) {
    domain.del(bookmarked)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

var invalidArticleId = function(response) {
    domain.get(Pathh)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidToken = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.invalid_user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidSecretCode = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.invalid_xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}
var invalidOS = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'andromax')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidVersion = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', 'A.B.C')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidType = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

module.exports = {
    getTopStories: getTopStories,
    getGeolocation: getGeolocation,
    getSourceFeed: getSourceFeed,
    getSearchXmax: getSearchXmax,
    getTrending: getTrending,
    getCategory: getCategory,
    invalidArticleId: invalidArticleId,
    postBookmark: postBookmark,
    unBookmark: unBookmark,
    invalidToken: invalidToken,
    invalidSecretCode: invalidSecretCode,
    invalidOS: invalidOS,
    invalidVersion: invalidVersion,
    invalidType: invalidType
};
