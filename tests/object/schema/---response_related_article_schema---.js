var response_feed_schema = {
    title: 'feed response body',
    type: 'object',
    items: {
        data: {
            title: 'feed response in data',
            type: 'object',
            properties: {
                title: 'feed content',
                type: 'object',
                properties:
                    {
                        id: { type: 'number' },
                        type: {
                            type: 'string',
                            enum: ['article', 'video']
                        },
                        title: { type: 'string' },
                        excerpt: { type: 'string' },
                        url: { type: 'string' },
                        timestamp: { type: 'number' },
                        thumbnails: {
                            title: 'thumbnails schema',
                            type: 'array',
                            properties:
                                {
                                    url: { type: 'string' },
                                    mime: { type: 'string' },
                                    width: { type: 'number' },
                                    height: { type: 'time' }
                                }
                        },
                        source: {
                            title: 'source schema',
                            type: 'object',
                            properties: {
                                id: { type: 'number' },
                                name: { type: 'string' },
                                url: { type: 'string' },
                                icon: { type: 'string' },
                            },
                            required: ['id', 'name', 'url', 'icon']
                        },
                        bookmarked: { type: 'boolean' },
                        short_url: { type: 'string' }
                    },
                additionalProperties: {
                    title: 'feed response ads dfp',
                    type: 'object',
                    properties: {
                        provider: {
                            type: 'string',
                            enum: ['DFP']
                        },
                        type: {
                            type: 'string',
                            enum: ['ads']
                        },
                    },
                    required: ['provider', 'type']
                },
                required: ['id', 'type', 'title', 'excerpt', 'url', 'timestamp', 'thumbnails', 'source', 'bookmarked', 'short_url']
            },
        },
        id: { type: 'string' },
        type: { type: 'string' },
        name: { type: 'string' },
        cursor: { type: 'string' },
        next_url: { type: 'string' }
    },
    required: ['data', 'id', 'type', 'name', 'cursor', 'next_url']
}
module.exports = response_feed_schema;
