var survey_schema = {
    title: 'survey schema',
    type: 'object',
    properties:
        {
            id: { type: 'string' },
            title: { type: 'string' },
            description: { type: 'string' },
            questions: { type: 'array'}
        },
    required: ['id', 'title', 'description', 'questions']
}

var questions_schema = {
        title: 'survey questions schema',
        type: 'object',
        properties: 
            {
                id: { type: 'string' },
                text: { type: 'string' },
                choices: { type: 'array'}
            },
            required: ['id', 'text', 'choices']
    }

var choices_schema = {
        title: 'survey choices schema',
        type: 'object',
        properties: 
            {
                id: { type: 'string' },
                text: { type: 'string' }
            },
            required: ['id', 'text']
    }
module.exports = {
    survey_schema: survey_schema,
    questions_schema: questions_schema,
    choices_schema: choices_schema
} 