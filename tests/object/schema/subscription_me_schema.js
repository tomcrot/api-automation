var subscription_me = {
    title: 'subscription me schema',
    type: 'object',
    properties:
        {
            type: { type: 'string',
                    enum: ['category'] },
            id: { type: 'string' },
            name: { type: 'string',
                    enum: ['Unik', 'Bola', 'Tekno', 'Celebs', 'Health', 'Oto', 'Style', 'Life', 'Travel', 'News', 'Asmara', 'Fun', 'Endeus', 'Celeb', 'Music']}
        },
    require: ['type', 'id', 'name']
}
module.exports = subscription_me;