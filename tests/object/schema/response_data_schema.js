var response_data_schema = {
    title: 'feed response in body data',
    type: 'object',
    items: {
        title: 'feed content',
        type: 'object',
        properties: {
            id: {
                description: 'the unique identifier for article',
                type: 'number'
            },
            type: { type: 'string' },
            title: { type: 'string' },
            excerpt: { type: 'string' },
            url: { type: 'string' },
            timestamp: { type: 'number' },
            thumbnails: {
                title: 'thumbnails schema',
                type: 'object',
                properties: {
                    url: { type: 'string' },
                    mime: { type: 'string' },
                    width: { type: 'number' },
                    height: { type: 'number' }
                }
            },
            source: {
                title: 'source schema',
                type: 'object',
                properties: {
                    id: { type: 'number' },
                    name: { type: 'string' },
                    url: { type: 'string' },
                    icon: { type: 'string' },
                },
                required: ['id', 'name', 'url']
            },
            bookmarked: { type: 'boolean' },
            short_url: { type: 'string' }
        },
        additionalProperties: {
            title: 'feed response ads dfp',
            type: 'object',
            properties: {
                provider: {
                    type: 'string',
                    enum: ['DFP']
                },
                type: {
                    type: 'string',
                    enum: ['ads']
                }
            },
            required: ['provider', 'type']
        }
    },
    required: ['id', 'type', 'title', 'excerpt', 'url', 'timestamp', 'thumbnails', 'source', 'bookmarked', 'short_url'],
}

module.exports = response_data_schema;
