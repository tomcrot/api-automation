var response_article_detail_schema = {
    title: 'article detail schema',
    type: 'object',
    properties: {
        id: {
            description: 'the unique identifier for article',
            type: 'number'
        },
        type: { type: 'string', enum: ['article']},
        title: { type: 'string'},
        excerpt: { type: 'string'},
        url: { type: 'string'},
        timestamp: { type: 'number'},
        thumbnails: {
            title: 'thumbnails schema',
            type: 'array',
            items: [
                {
                url: { type: 'string' },
                mime: { type: 'string' },
                width: { type: 'number' },
                height: { type: 'number' }
            }]
        },
        source: {
            title: 'source schema',
            type: 'object',
            properties: {
                id: { type: 'number' },
                name:{type: 'string'},
                url: { type: 'string' },
                icon: { type: 'string' },
            },
            required: ['id', 'name', 'url']
            },
        bookmarked: { type: 'boolean'},
        content: 
            {
            title: 'content schema',
            type: 'array',
            items: {
                properties:{
                    text: { type: 'string'}, 
                    type: { type: 'string', enum: ['text', 'video', 'image']}
                }
        },
    },
        short_url: {type: 'string'},
    },
    required: ['id', 'type', 'title', 'excerpt', 'url', 'timestamp', 'source', 'bookmarked', 'content', 'short_url']
}
module.exports =  response_article_detail_schema;