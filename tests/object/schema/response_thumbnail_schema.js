var thumbnails = {
    title: 'thumbnails schema',
    type: 'array',
    properties:[
        {
            url: { type: 'number' },
            mime: { type: 'string' },
            width: { type: 'number' },
            height: { type: 'number' }
        }],
        required: ['urls']
}
module.exports = thumbnails;
