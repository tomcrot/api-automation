var response_native_polling = {
    title: 'native poling schema',
    type: 'object',
    properties: {
        id: { type: 'string' },
        text: { type: 'string' },
        choices: {
            title: 'choose option',
            type: 'object',
            properties:
                    {
                        id: { type: 'string' },
                        text: { type: 'string' },
                        is_voted: { type: 'boolean' },
                        count: { type: 'number' }
                    },
            required: ['id', 'text', 'is_voted','count']
        },
        endtime: { type: 'number' },
        total_vote: { type: 'number' },
        // type: { type: 'string' },
        template: { type: 'string' },
    },
    required: ['id', 'text', 'choices', 'endtime', 'total_vote', 'type', 'template']
}
module.exports = response_native_polling;