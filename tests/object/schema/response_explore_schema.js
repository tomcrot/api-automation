var explore = {
    title: 'explore schema',
    type: 'object',
    properties:[
        {
            featured: {
                title: 'featured schema',
                type: 'object',
                properties: {
                    data: {
                        title: 'featured data schema',
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                type: { type: 'string',
                                        enum: ['category', 'source']},
                                name: { type: 'string'},
                                id: { type: 'number'},
                                image: {
                                    title: 'image schema',
                                    type: 'object',
                                    properties: {
                                        url: { type: 'string'},
                                        width: { type: 'number'},
                                        height: { type: 'number'},
                                        mime: { type: 'string'}
                                    },
                                    required: ['url', 'width', 'height', 'mime']
                                },
                            },
                            required: ['type', 'name', 'id', 'image']
                        },
                    },
                    required: ['data']
                }
            },
            topic_groups: {
                title: 'topic groups schema',
                type: 'object',
                properties: {
                    data: {
                        title: 'topic groups data schema',
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                id: { type: 'number'},
                                name: { type: 'string'}
                            },
                            required: ['id', 'name']
                        },
                    },
                    required: ['data']
                },
            }
        }],
        required: ['featured', 'topic_groups']
}
module.exports = explore;
