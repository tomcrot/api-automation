var search_recommendation = {
    title: 'search recommendation schema',
    type: 'object',
    properties: {
        data: {
            title: 'search recommendation data schema',
            type: 'object',
            properties: {
                text: { type: 'string'}
            },
            required: ['text']
        },
        required: ['data']
    }
}
module.exports =  search_recommendation;
