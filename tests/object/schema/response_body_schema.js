var response_body_schema = {
    title: 'feed response body',
    type: 'object',
    properties: {
        data: { type: 'array'},
        id: { type: 'string'},
        type: { type: 'string'},
        name: { type: 'string'},
        cursor: { type: 'string'},
        next_url: { type: 'string'}
    },
    required: ['data','id', 'type', 'name', 'cursor', 'next_url']
}
module.exports =  response_body_schema;
