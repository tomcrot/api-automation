var notification_schema = {
    title: 'notification body schema',
    type: 'object',
    properties: {
        data: {
            title: 'notification data schema',
            type: 'object',
            properties: {
                objectId: { type: 'number'},
                type: { type: 'string', 
                        enum: ['article']},
                breaking: { type: 'boolean'},
                alert: { type: 'string'},
                excerpt: { type: 'string'},
                target: { type: 'string'},
                image: { type: 'string'},
                axis: {
                    title: 'notification axis schema',
                    type: 'object',
                    properties: {
                        type: { type: 'string'},
                        id: { type: 'number'}
                    },
                    required: ['type', 'id']
                },
                push_hash: { type: 'number'},
                pushedAt: { type: 'string'}
            },
            required: ['objectId', 'type', 'breaking', 'alert', 'excerpt', 'target', 'image', 'axis', 'push_hash', 'pushedAt']
        }
    }
}
module.exports = notification_schema;