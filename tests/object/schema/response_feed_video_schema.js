var response_feed_video_schema = {
    title: 'video feed response',
    type: 'object',
    properties: {
        id: { type: 'string' },
        provider: {
            type: 'string',
            enum: ['youtube', 'facebook']
        },
        title: { type: 'string' },
        description: { type: 'string' },
        original_video: {
            title: 'original video block',
            type: 'object',
            properties: {
                id: { type: 'string' },
                url: { type: 'string' }
            },
            required: ['id', 'url']
        },
        thumbnail: { type: 'string' },
        categories: { type: 'array' },
        channel: {
            title: 'chennel blcok',
            type: 'object',
            properties: {
                id: { type: 'string' },
                name: { type: 'string' },
                icon: { type: 'string' },
                url: { type: 'string' }
            },
            required: ['id', 'name', 'icon', 'url']
        },
        publish_time: { type: 'number' },
        duration: { type: 'number' },
        stats: {
            title: 'stats block',
            type: 'object',
            properties: {
                comments: { type: 'number' },
                dislikes: { type: 'number' },
                likes: { type: 'number' },
                shares: { type: 'number' },
                views: { type: 'number' }
            },
            required: ['comments', 'dislikes', 'likes', 'shares', 'views']
        },
        type: {
            type: 'string',
            enum: ['video']
        },
        short_url: { type: 'string' }
    },
    required: ['id', 'provider', 'title', 'description', 'original_video', 'thumbnail','categories','channel','publish_time', 'duration', 'stats', 'type', 'short_url']
}
module.exports = response_feed_video_schema;
