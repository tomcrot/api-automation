var forgot_password = {
    title: 'forgot password schema',
    type: 'object',
    properties:
       {
            data: {
                title: 'forgot password data',
                type: 'object',
                properties: {
                    success: { type: 'boolean'}
                }
            },
            required: ['data']
    }
}
module.exports = {
    forgot_password: forgot_password
} 