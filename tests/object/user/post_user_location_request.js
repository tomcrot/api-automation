const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const env = require('dotenv').config();
const domain = supertest(process.env.API_BASE_URL);

var Path = "/user/location";

var globals = require('./../../object/globals.js');

var postAPI = function(body, response) {
    domain.post(Path)
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

module.exports = {
    postAPI: postAPI
};
