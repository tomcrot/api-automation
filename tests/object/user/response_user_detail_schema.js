var user_detail_schema = {
    title: 'user detail schema',
    type: 'object',
    properties: {
        id: { type: 'number' },
        name: { type: 'string' },
        email: { type: 'string' },
        join_at: { type: 'number' },
        profile_picture: { type: 'string' },
        register_method: {
            title: 'register method schema',
            type: 'object',
            properties: {
                provider: { type: 'string' },
                id: { type: 'string' }
            },
            required: ['provider', 'id']
        },
        is_guest: { type: 'boolean' },
        token: {
            title: 'token schema',
            type: 'object',
            properties: {
                access_token: { type: 'string' },
                token_type: { type: 'string' },
                expires_in: { type: 'number' }
            },
            required: ['access_token', 'token_type', 'expires_in']
        },
        subscriptions: {
            title: 'subscription schema',
            type: 'array'
        },
        referral_link: { type: 'string' },
        show_ads: { type: 'boolean' },
        location: {
            title: 'location schema',
            type: 'object',
            properties: {
                latitude: { type: 'number' },
                longitude: { type: 'number' },
                id: { type: 'number' }
            },
            required: ['latitude', 'longitude', 'id']
        }

    },
    required: ['id', 'name', 'email', 'join_at', 'profile_picture', 'register_method', 'is_guest', 'token', 'subscriptions', 'referral_link', 'show_ads', 'location']
}

var subscription_detail = {
        title: 'subscription schema',
        type: 'object',
        properties:
            {
                type: {
                    type: 'string',
                    enum: ['category']
                },
                id: { type: 'string' },
                name: { type: 'string' }
            },
        required: ['type', 'id', 'name']
}
module.exports = {
    user_detail_schema: user_detail_schema,
    subscription_detail: subscription_detail
};
