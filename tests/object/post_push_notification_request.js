const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const env = require('dotenv').config();
const domain = supertest(process.env.GCM);

var Path = "/gcm/send";

var postAPI = function(body, response) {
    domain.post(Path)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'key=AAAA2tz292Y:APA91bEqJKzzM7Jg6uWLnvLAoEPFEZsLyGb6-untN9Pi20wsmh_WS4RFc38JyyA5ZcUAcjciBaLQ9SvJ6uqIRouwm96M5HaijETkm9Gn6TcmotqqhzILM1Nd7LfiDU7HgjPB55fezGOg')
        .end(function(err, result) {
            response(result);
          })
}

module.exports = {
    postAPI: postAPI

};
