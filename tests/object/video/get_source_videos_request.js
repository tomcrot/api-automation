const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const env = require('dotenv').config();
const domain = supertest(process.env.API_BASE_URL);

var Path = "/videofeed/source:2";
var globals = require('./../../object/globals.js');

var getAPI = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })

}

var getInvalidToken = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.invalid_user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidSecretCode = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.invalid_xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}
var invalidOS = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'andromax')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidVersion = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', 'A.B.C')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var invalidType = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyType = function(response) {
    domain.get(Path)
        // .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyAccept = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        // .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyClientId = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        // .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyClientSecret = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        // .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyOs = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        // .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyVersion = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        // .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

var emptyToken = function(response) {
    domain.get(Path)
        .set('Content-Type', 'application/xml')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        // .set('Authorization', globals.user_token)
        .end(function(err, result) {
            response(result);
          })
}

module.exports = {
    getAPI: getAPI,
    getInvalidToken: getInvalidToken,
    invalidSecretCode: invalidSecretCode,
    invalidOS: invalidOS,
    invalidVersion: invalidVersion,
    invalidType: invalidType,
    emptyType: emptyType,
    emptyAccept: emptyAccept,
    emptyClientId: emptyClientId,
    emptyClientSecret: emptyClientSecret,
    emptyOs: emptyOs,
    emptyVersion: emptyVersion,
    emptyToken: emptyToken
};
