const should = require('chai').should();
const expect = require('chai').expect;
const supertest = require('supertest');
const env = require('dotenv').config();
const domain = supertest(process.env.API_BASE_URL);

var Path = "/auth/guest";
var LoginEmail = "/auth/login";
var SignUp = "/auth/signup";
var LoginTwitter = "/auth/social/twitter"
var LoginFacebook = "/auth/social/facebook"

var globals = require('./../../object/globals.js');

var postAPI = function(body, response) {
    domain.post(Path)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

var postLoginEmail = function(body, response) {
    domain.post(LoginEmail)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

var postSignUp = function(body, response) {
    domain.post(SignUp)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

var postLoginTwitter = function(body, response) {
    domain.post(LoginTwitter)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}

var postLoginFacebook = function(body, response) {
    domain.post(LoginFacebook)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('X-Kurio-Client-ID', '2')
        .set('X-Kurio-Client-Secret', globals.xsecret)
        .set('X-OS', 'android')
        .set('X-App-version', '3.7.0')
        .set('Authorization', globals.user_token)
        .send(body)
        .end(function(err, result) {
            response(result);
          })
}


var token_uuid = ({'device_token': 'dVEI5Pv7Udg:APA91bG4e1EaBEV99O7mjP5SGrvmXOrloknqOOWgMHRnknT-LnXFSAGI57Sd7M5KU3K1b6_r22aeMPLCdqhDcUyQ3mhwWLH0G69e8Ou5-ZZJqMqEwLSfCGZzv6sUNLZ5v33oPJb_M6mi', 'uuid': 'a9016a1fefa4050cd'})

module.exports = {
    postAPI: postAPI,
    token_uuid: token_uuid,
    postLoginEmail: postLoginEmail,
    postSignUp: postSignUp,
    postLoginTwitter: postLoginTwitter,
    postLoginFacebook: postLoginFacebook

};
