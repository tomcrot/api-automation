// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = require('chai').expect;
const header = require('./../../object/user/post_user_location_request.js');

var testData = {
    "latitude": 6.190157, 
    "longitude": 106.798631
}
describe('POST request ./post', function () {

    describe('POST request store user location', function () {
        it('Request store user location', function (done) {
            header.postAPI(testData, function (response) {
                console.log(response.body);
                console.log(testData);
                console.log(header);
                expect(response.status).to.equal(204);
                done();
            });
        });
    });
})
