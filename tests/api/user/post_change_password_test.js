// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = require('chai').expect;
const header = require('./../../object/user/post_change_password_request');
var forgotPasswordSchema = require('./../../object/schema/forgot_password_schema');

var testData = ({
    "old_password": "newpass",
    "new_password": "newpasssss"
  })
describe('POST request ./user/changepassword', function () {
        it('Verify if API is able to change password', function (done) {
            header.postAPI(testData, function (response) {
                console.log(response.body);
                expect(response.status).to.equal(200);
                // expect(response.body).to.be.jsonSchema(forgotPasswordSchema.forgot_password)
                done();
        });
    });
})
