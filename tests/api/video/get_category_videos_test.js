// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = chai.expect
const header = require('./../../object/video/get_category_videos_request.js');
var responseVideoFeed = require('./../../object/schema/response_feed_video_schema.js')

describe('GET Request ./videofeed/category:59', function() {
      this.timeout(10000)
        it('verify if API is able to get video feed base on category id', function (done) {
            header.getAPI
            header.getAPI(function(response) {
                console.log(response.body.data[0]);
                expect(response.status).to.equal(200);
                expect(response.body.data[0]).to.be.jsonSchema(responseVideoFeed)
                done();
            });
        });
        it('Should not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });

        it('Should not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('Should not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('Should not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('Should not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('Should not authorized if content-type is not set', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('Should not authorized if Accept json is not set', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('Should not authorized if client id is not set', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('Should not authorized if client secret is not set', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('Should not authorized if OS name is not set', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('Should not authorized if version app is not set', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('Should not authorized if token is not set', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            });
        });
});
