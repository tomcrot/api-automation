const should = require('chai').should();
const expect = require('chai').expect;
const header = require('./../../object/article/post_bookmark_request.js');

var body = ({
    "device_token":"APA91bFJP7BHtlNDT_icK9qH2EJpBUIy2XEo44uBtOhGAhB1KvOhdivyZJYtXNS8lV_CLAqSQl1HnC-V5xd-G8GwJW1lI95utJjgYFLICgKVT7re5i9tlGLHYgZ54P901Ge2mylKu6EE",
    "uuid":"a9016a1fefa4050c"
  })

describe('POST request ./article/5145881/bookmark', function() {

    describe('Request @post', function() {
        it('verify if user is able to bookmark article', function(done) {
            header.postAPI(body, function(response) {
                console.log(response.body);
                expect(response.status).to.equal(201);
                expect(response.body).to.have.property('article_id')
                expect(response.body).to.have.property('bookmarked')
                done();
            });
        });
    });

})
