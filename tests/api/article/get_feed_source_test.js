// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = chai.expect
const header = require('./../../object/article/get_source_feed_request');
var responseFeedSchema = require('./../../object/schema/response_feed_schema.js');
var messageSchema = require('./../../object/schema/message_schema')

describe('GET Request ./feed/source:58', function() {

    describe('get feed from defined source', function() {
      this.timeout(10000)
        it('check schema for json response on source viva.co.id feed', function (done) {
            header.getAPI(function(response) {
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(200);
                expect(response.body).to.be.jsonSchema(responseFeedSchema)
                done();
            });
        });
        it('check for ads', function(done) {
            header.getAPI(function(response) {
                // console.log(response.body.data[2])
                console.log(response.status);
                expect(response.status).to.equal(200);
                expect(response.body.data[2]).to.be.jsonSchema(responseFeedSchema.items.data.properties.additionalProperties)
                done();
            });
        });
        it('verify for thumbnails response', function(done) {
            header.getAPI(function(response) {
                // console.log(response.body.data[0].thumbnails)
                console.log(response.status);
                expect(response.status).to.equal(200)
                expect(response.body.data[0].thumbnails).to.be.jsonSchema(responseFeedSchema.items.data.properties.properties.thumbnails)
                done();
            });
        });

        it('verify if server will response 404 code if source id is invalid - /feed/source:123AB', function(done){
            header.invalidIdSource(function(response){
                // console.log(response.body);
                console.log(response.status);
                // expect(response.status).to.equal(404);
                done();
            })
        });
        
        it('verify if user is not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });

        it('verify if user is not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if content-type is empty', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if Accept json is empty', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client id is empty', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client secret is empty', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if OS name is empty', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version app is empty', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if token is empty', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
    });

})
