// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = chai.expect
const header = require('./../../object/article/get_top_stories_request.js');
var responseFeedSchema = require('./../../object/schema/response_feed_schema.js')
var nativePollingSchema = require('./../../object/schema/native_polling_schema.js')

describe('GET Request ./feed/top_stories', function () {

    describe('Request @get', function () {
        this.timeout(5000)
        it('check the ads', function (done) {
            header.getAPI(function (response) {
                // console.log(response.body.data[3])
                expect(response.body.data[2]).to.be.jsonSchema(responseFeedSchema.items.data.properties.additionalProperties)
                done();
            });
        });    
        it('get top stories feed', function (done) {
            header.getAPI(function (response) {
                // console.log(response.body)
                expect(response.status).to.equal(200)
                expect(response.body).to.be.jsonSchema(responseFeedSchema)
                done();
            });
        });
        it('check the thumbnails', function (done) {
            header.getAPI(function (response) {
                // console.log(response.body.data[0].thumbnails)
                expect(response.body.data[0].thumbnails).to.be.jsonSchema(responseFeedSchema.items.data.properties.properties.thumbnails)
                done();
            });
        });
        it('check the source', function (done) {
            header.getAPI(function (response) {
                // console.log(response.body.data[0].source)
                expect(response.body.data[0].source).to.be.jsonSchema(responseFeedSchema.items.data.properties.properties.source)
                done();
            });
        });
        it('check the native polling', function (done) {
            header.getAPI(function (response) {
                // console.log(response.body.data[1])
                expect(response.body.data[1]).to.be.jsonSchema(nativePollingSchema.properties)
                done();
            });
        });
        it('check the native polling for choice option', function (done) {
            header.getAPI(function (response) {
                // console.log(response.body.data[1].choices)
                expect(response.body.data[1]).to.be.jsonSchema(nativePollingSchema.properties.choices.properties)
                done();
            });
        });
        it('verify if user is not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content-type is empty', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if Accept json is empty', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client id is empty', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client secret is empty', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if OS name is empty', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version app is empty', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if token is empty', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
    });
});
