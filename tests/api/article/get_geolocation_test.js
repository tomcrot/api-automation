const should = require('chai').should();
const expect = require('chai').expect;
const header = require('./../../object/article/get_geolocation_request.js');

describe('GET Request ./feed/geolocation', function() {

    describe('Request @get', function() {
      this.timeout(10000)
        it('get geolcation', function (done) {
            header.getAPI(function(response) {
                // console.log(response.body);
                expect(response.status).to.equal(200);
                
                done();
            });
        });
        it('verify if user is not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });

        it('verify if user is not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content-type is empty', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if Accept json is empty', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client id is empty', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client secret is empty', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if OS name is empty', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version app is empty', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if token is empty', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
    });

})
