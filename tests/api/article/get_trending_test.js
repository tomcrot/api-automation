// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = require('chai').expect;
const header = require('./../../object/article/get_trending_request.js');
var responseFeedSchema = require('./../../object/schema/response_feed_schema.js');

describe('GET Request ./feed/trend:15', function() {

    describe('Request trending end point', function() {
      this.timeout(10000)
        it('get trending id 15', function (done) {
            header.getAPI
            header.getAPI(function(response) {
                console.log(response.body);
                expect(response.status).to.equal(200);
                expect(response.body).to.be.jsonSchema(responseFeedSchema)
                done();
            });
        });
        // it('verify for thumbnails response', function(done) {
        //     header.getAPI
        //     header.getAPI(function(response) {
        //         console.log(response.body)
        //         expect(response.status).to.equal(200)
        //         expect(response.body.data[0].thumbnails).to.be.jsonSchema(responseFeedSchema.items.data.properties.properties.thumbnails)
        //         done();
        //     });
        // });
        // it('verify for dfp ads response', function(done) {
        //     header.getAPI
        //     header.getAPI(function(response) {
        //         // console.log(response.body.data[2])
        //         expect(response.status).to.equal(200)
        //         expect(response.body.data[2]).to.be.jsonSchema(responseFeedSchema.items.data.properties.additionalProperties)
        //         done();
        //     });
        // });
        it('verify if user is not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content-type is empty', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if Accept json is empty', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client id is empty', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client secret is empty', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if OS name is empty', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version app is empty', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if token is empty', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
    });

})
