// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = chai.expect
const header = require('./../../object/article/get_related_article_request.js');
var responseFeedSchema = require('./../../object/schema/response_feed_schema.js');
var responseBodySchema = require('./../../object/schema/response_body_schema.js');
var responseThumbnailSchema = require('./../../object/schema/response_thumbnail_schema.js')

describe('GET Request ./article/6588327/related?feed=top_stories', function() {

    describe('get feed from related', function() {
      this.timeout(10000)
      it('verify if feed response from related topics is corresponding to requirement', function (done) {
        header.getAPI(function(response) {
            // console.log(response.body[0]);
            console.log(response.status)
            expect(response.status).to.equal(200)
            expect(response.body[0]).to.be.jsonSchema(responseBodySchema)
            expect(response.body[0]).to.have.property('type', 'related')
            expect(response.body[0]).to.have.property('name', 'Related Topics')
            // console.log(response.body[0].data);
            expect(response.body[0].data).to.be.jsonSchema(responseFeedSchema.items.data)
            done();
        });
    });
    it('verify if feed response from related category is corresponding to requirement', function (done) {
        header.getAPI(function(response) {
            // console.log(response.body[1]);
            console.log(response.status);
            expect(response.status).to.equal(200)
            expect(response.body[1]).to.be.jsonSchema(responseBodySchema)
            expect(response.body[1]).to.have.property('type', 'top_stories')
            expect(response.body[1]).to.have.property('name', 'Top Stories')
            done();
        });
    });
    it('verify if feed response from related source is corresponding to requirement', function (done) {
        header.getAPI(function(response) {
            // console.log(response.body[2]);
            console.log(response.status);
            expect(response.status).to.equal(200)
            expect(response.body[2]).to.be.jsonSchema(responseBodySchema)
            expect(response.body[2]).to.have.property('type', 'source')
            done();
        });
    });
    it('verify for thumbnails response', function(done) {
        header.getAPI(function(response) {
            // console.log(response.body[0].data[0].thumbnails)
            console.log(response.status);
            expect(response.status).to.equal(200)
            expect(response.body[0].data[0].thumbnails).to.be.jsonSchema(responseThumbnailSchema.properties)
            done();
            });
        });
        it('verify if user is not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });

        it('verify if user is not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if content-type is empty', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if Accept json is empty', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client id is empty', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client secret is empty', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if OS name is empty', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version app is empty', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if token is empty', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
    });
})
