const should = require('chai').should();
const chai = require('chai')
// chai.use(require('chai-json-schema'))
const expect = require('chai').expect;
const header = require('./../../object/authorization/post_email_signup_request');

var bodyData = ({
        "name" : "rambo",
        "device_token" : "dwbG9-b0MRA:APA91bFNVud9JGUSuO29nDc_gwoKE0-zX0W0gPHfEaB5OwJMYeMfyMbUWU3hRdqE7xX2yKXftEOW3n_0cTqENLf8SEkk0GAkngI11N7eVLa01V-5GhWtLtsKY1L-zUYRkKCX1sCZLY73",
        "uuid" : "a9016a1fefa4050c",
        "email" : "tomy@kurio.co.id",
        "password" : "qwerty"
      });

describe('POST request signup by email', function () {

    describe('Request @post', function () {
        it('Request signup by email', function (done) {
            header.postAPI(bodyData, function (response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(200);
                done();
            });
        });
        it('should 409 response and message if email has already been used', function(done) {
            header.postAPI(bodyData, function(response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(409);
                expect(response.body).to.have.property('message', 'Email has already been used')
                done();
            });
        });
    });

})
