const should = require('chai').should();
const expect = require('chai').expect;
const header = require('./../../object/authorization/delete_register_device_request.js');

var body = ({
    "device_token" : "ckwtgVm81gY:APA91bGbTJwgiVlFculi2lAvPVQR6CDd4_prjOEfu8a3fl74bObBoT3YOBUeaEcOByOTOqKszXvLjLzRLpuSX-5Acch6WNaJrM9ZjGEagtzyD8oQmKgHBT8I7rtgsGA85O1j9jg4VsHP",
    "uuid" : "137a97b759fdac47"
  })

describe('Request DELETE', function() {

    describe('Request @delete register device', function() {
        it('Should delete the registered device with http status 204', function(done) {
            header.deleteAPI(body, function(response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(204);
                done();
            });
        });
    });

})
