const should = require('chai').should();
const expect = require('chai').expect;
const header = require('./../../object/authorization/post_twitter_auth_request.js');
var assertMessage = require('./../../object/schema/message_schema.js');

var body = ({
    "oauth_token": "890472995491270656-WTsOWFhrK40GrHWQ72rWUmj1PtUU8Xs",
    "oauth_token_secret": "xL8tOHtwVwpCWHjoXsBZSUg82PS3XIraqbe671joYsKlG",
    "device_token": "eVcPNxqJDpE:APA91bHsvv_dU3MSQBcF4y6BLmi1Ih8U299U6GIK2xCIkwCXsIOu9fbOITiMyMBYpd0ToGrf1S4L6fuiACFSRRWvPbgfWmYNyloiI-wkc6gTJcCkkztaWEXERhIAtD_DnSNDqEdfPG2P",
    "uuid": "a9016a1fefa4050c"
  })

describe('POST request twitter authorization', function() {

    describe('Request @post', function() {
        it('Request login by twitter account', function(done) {
            header.postAPI(body, function(response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(200);
                done();
            });
        });
        it('should 400 response and message you have connected', function(done) {
            header.postAPI(body, function(response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(400);
                expect(response.body).to.have.property('message', 'You have to connnect to facebook account')
                done();
            });
        });
    });

})
