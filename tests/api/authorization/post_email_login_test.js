const should = require('chai').should();
const chai = require('chai')
// chai.use(require('chai-json-schema'))
const expect = require('chai').expect;
const header = require('./../../object/authorization/post_email_login_request');

var bodyData = (
    {
    "device_token" : "dFVIePmLYRU:APA91bEsPwuJwutqI226C1Vi2tm-yoW0nkTm59N6FNlL3OPgK5TYxj7d6SK9U5WanoOrAl_ZfIiGK7i8EWArfVoRHBcrDMhpvfTYk6g-IZ2R_jHkjnISrKx7RVFvIAvrMom1kDB7KnC3",
    "uuid": "a9016a1fefa4050c",
    "email": "ses.kamling@gmail.com",
    "password": "123456"
});

describe('POST request login by email', function () {

    describe('Request @post', function () {
        it('Request login by email', function (done) {
            header.postAPI(bodyData, function (response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(200);
                done();
            });
        });
        it('should 400 response and message you have connected', function(done) {
            header.postAPI(bodyData, function(response) {
                console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(400);
                expect(response.body).to.have.property('message', 'You have to connnect to facebook account')
                done();
            });
        });
    });

})
