// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = chai.expect
const header = require('./../../object/explore/get_explore_request.js');
var responseExplore = require('./../../object/schema/response_explore_schema.js');

describe('GET Request ./explore', function() {
      this.timeout(10000)
        it('verify if API is able to get explore feed', function (done) {
            header.getAPI
            header.getAPI(function(response) {
                // console.log(response.body);
                // expect(response.status).to.equal(200);
                // expect(response.body).to.be.jsonSchema(responseExplore)
                // console.log(response.body.featured);
                // expect(response.body.featured).to.be.jsonSchema(responseExplore.properties[0].featured.properties)
                // console.log(response.body.featured.data[0]);
                // expect(response.body.featured.data[0]).to.be.jsonSchema(responseExplore.properties[0].featured.properties.data.items)
                // console.log(response.body.topic_groups);
                // expect(response.body.topic_groups).to.be.jsonSchema(responseExplore.properties[0].topic_groups.properties)
                console.log(response.body.topic_groups.data[0]);
                expect(response.body.topic_groups.data[0]).to.be.jsonSchema(responseExplore.properties[0].topic_groups.properties.data.items)
                done();
            });
        });
        it('verify if user is not authorized if token is invalid and will get 401 response', function(done){
            header.getInvalidToken(function(response){
                // console.log(response.body);
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });

        it('verify if user is not authorized if secret code is invalid', function(done){
            header.invalidSecretCode(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if os is invalid', function(done){
            header.invalidOS(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if version is invalid', function(done){
            header.invalidVersion(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if content type is invalid', function(done){
            header.invalidType(function(response){
                // console.log(response.body);
                console.log(response.status);
                done();
            })
        });
        it('verify if user is not authorized if content-type is empty', function(done){
            header.emptyType(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if Accept json is empty', function(done){
            header.emptyAccept(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client id is empty', function(done){
            header.emptyClientId(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if client secret is empty', function(done){
            header.emptyClientSecret(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if OS name is empty', function(done){
            header.emptyOs(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if version app is empty', function(done){
            header.emptyVersion(function(response){
                console.log(response.status);
                // expect(response.status).to.equal(401);
                done();
            })
        });
        it('verify if user is not authorized if token is empty', function(done){
            header.emptyToken(function(response){
                console.log(response.status);
                expect(response.status).to.equal(401);
                done();
            })
        });
})
