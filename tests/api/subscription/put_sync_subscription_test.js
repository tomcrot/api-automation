// const should = require('chai').should();
const chai = require('chai')
chai.use(require('chai-json-schema'))
const expect = chai.expect
const header = require('./../../object/subscription/put_sync_subscription_request.js');
var subscriptionMe = require('./../../object/schema/subscription_me_schema.js');

var testData = ([
    {
        "type": "category",
        "id": "59",
        "name": "Unik"
    },
    {
        "type": "category",
        "id": "51",
        "name": "Bola"
    },
    {
        "type": "category",
        "id": "40",
        "name": "Tekno"
    },
    {
        "type": "category",
        "id": "58",
        "name": "Celebs"
    },
    {
        "type": "category",
        "id": "76",
        "name": "Health"
    },
    {
        "type": "category",
        "id": "55",
        "name": "Oto"
    },
    {
        "type": "category",
        "id": "64",
        "name": "Style"
    },
    {
        "type": "category",
        "id": "63",
        "name": "Life"
    },
    {
        "type": "category",
        "id": "65",
        "name": "Travel"
    },
    {
        "type": "category",
        "id": "99",
        "name": "News"
    },
    {
        "type": "category",
        "id": "79",
        "name": "Asmara"
    },
    {
        "type": "category",
        "id": "70",
        "name": "Cooking"
    }
])

describe('GET Request ./subscription/me', function() {

    describe('Request @get', function() {
        it('get subscription me list topic for default', function (done) {
            header.putAPI(testData, function(response) {
                console.log(response.body)
                expect(response.status).to.equal(200);
                expect(response.body[0]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[1]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[2]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[3]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[4]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[5]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[6]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[7]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[8]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[9]).to.be.jsonSchema(subscriptionMe)
                expect(response.body[10]).to.be.jsonSchema(subscriptionMe)
                done();
            });
        });
        it('verify if user is not authorized if token is invalid');
        it('verify if user is not authorized if secret code is invalid');
        it('verify if user is not authorized if os is invalid');
        it('verify if user is not authorized if version is invalid');
        it('verify if user is not authorized if content type is invalid');
    });

})
